# Vista
## About
A database and genome assembly search tool for typing _Vibrio cholerae_ and identifying virulence genes and clusters.

This tool is currently under development by the [CGPS](https://www.pathogensurveillance.net/). Please open an issue or contact us via [email](mailto:pathogenwatch@cgps.group) if you would link to know more or contribute.

## Licensing
See [LICENSE](LICENSE).
